import math
import re
import logging

from selenium.webdriver.common.by import By


# NOTE: All in lowercase !IMPORTANT!
TAG_ID = {
    'virtual reality': 154,
    'robots': 41,
    'innovation': 128
}


def get_projects_by_tag(driver, tag):
    if isinstance(tag, str):
        tag = TAG_ID[tag.lower()]

    assert isinstance(tag, int)

    driver.get(f'https://www.kickstarter.com/discover/advanced?tag_id={tag}')

    _projects_count = driver.find_element(By.XPATH, '//*[contains(@class, "count")]')
    projects_count = int(re.match('(\d+) projec.+', _projects_count.text).group(1))
    projects_per_page = len(driver.find_elements(By.XPATH, '//*[contains(@class, "js-react-proj-card")]'))

    pages_count = math.ceil(projects_count / projects_per_page)
    
    for page in range(1, pages_count):
        driver.get(f'https://www.kickstarter.com/discover/advanced?tag_id={tag}&page={page}')

        blocks = driver.find_elements(By.XPATH, '//*[contains(@class, "js-react-proj-card")]//a[contains(@class, "block")][contains(@class, "img-placeholder")]')
        projects_links = [block.get_attribute('href') for block in blocks]

        for proj_link in projects_links:
            url = _get_project_link(driver, proj_link)

            if url is not None:
                yield url

def _get_project_link(driver, project_url):
    assert isinstance(project_url, str) 
    assert project_url.startswith('http')

    driver.get(project_url)

    try:
        website_btn = driver.find_element(By.XPATH, '//*[contains(@class, "project-profile__button")][contains(@class, "btn")]')
    except:
        return None

    return website_btn.get_attribute('href')

