from collections import namedtuple

from selenium.webdriver.common.by import By

import urls_fetch.kickstarter as kickstarter
from log import fetch_logger as logger


Fetcher = namedtuple('Fetcher', ['func', 'category', 'source'])

FETCHERS_LIST = []

def fetcher(category, source):
    def decorator(f):
        def wrapper(*args, **kw):
            logger.debug(f'Started fetcher: category={category}, source={source}')
            return f(*args, **kw)

        FETCHERS_LIST.append(Fetcher(wrapper, category, source))
        return wrapper
    return decorator

@fetcher('ico', 'icobazaar')
def get_ico_sites(driver):
    '''
    1. GET icobazaar.com
    2. Click on button 'All' to disable filtering
    3. Get url's of intermediate pages (ico profile of icobazaar.com)
    '''

    driver.get('http://icobazaar.com/v2/list/featured')

    logger.debug('Website opened')

    btn_all = driver.find_element(By.XPATH, '//*[@class="filter-item"][last()]//*[contains(@class, "btn")]')
    btn_all.click()

    logger.debug('Clicked on "All" button')

    _links = [el.get_attribute('href') for el in driver.find_elements(By.XPATH, '//*[@class="cell-link"]')]

    logger.debug(f'Found {len(_links)} items')

    for link in _links:
        driver.get(link)
        logger.debug(f'Opened page: {link}') 

        url = driver.find_element(By.XPATH, '//a[text() = "ICO website"]').get_attribute('href')
        logger.debug(f'Yielded new url: {url}')

        yield url

@fetcher('banks', 'wikipedia')
def get_banks_wikipedia(driver):
    driver.get('https://en.wikipedia.org/wiki/List_of_banks_in_Russia')

    links = [el.get_attribute('href') for el in driver.find_elements(By.XPATH, '//tr/td[position() = 4]/a')]
 
    for link in links:
        yield link

@fetcher('vr_startups', 'kickstarter')
def kickstarter_vr(driver):
    return kickstarter.get_projects_by_tag(driver, 'Virtual Reality')

@fetcher('robots_startups', 'kickstarter')
def kickstarter_robots(driver):
    return kickstarter.get_projects_by_tag(driver, 'Robots')

@fetcher('innovations_startups', 'kickstarter')
def kickstarter_innovations(driver):
    return kickstarter.get_projects_by_tag(driver, 'Innovation')
