from argparse import ArgumentParser, ArgumentError
import os

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.common.exceptions import StaleElementReferenceException

from db import Site
from settings import BROWSER_SIZE, CSS_PROPS, USER_AGENT
from urls_fetch import FETCHERS_LIST
import screener


def parse_args():
    def type_ignore(arg):
        splitted = str(arg).split(':')

        if len(splitted) != 2:
            raise ArgumentError()

        return splitted
    
    parser = ArgumentParser()
    subparsers = parser.add_subparsers(dest='mode')
    subparsers.required = True

    parser_fetch_urls = subparsers.add_parser('fetch_urls')
    parser_fetch_urls.add_argument(
        '--ignore',
        nargs='+',
        type=type_ignore,
        help='Ignore specified fetchers. Format: SOURCE:CATEGORY (e.g. wikipedia:banks)'
    )
    parser_fetch_urls.add_argument(
        '--dry-run',
        action='store_true',
        default=False,
        help='Only print all actions. No changes will be applyed'
    )
    
    parser_screen = subparsers.add_parser('screen')
    parser_screen.add_argument(
        '--no-scroll-bottom',
        action='store_true',
        help='Disable scroll to bottom feature (for js animation triggering)'
    )
    parser_screen.add_argument(
        '--no-mark-saved',
        action='store_true',
        help='Disable marking sites as screened (in mongo)'
    )

    parser_screen_subparsers = parser_screen.add_subparsers(dest='screen_mode')
    parser_screen_subparsers.required = True

    parser_screen_sliding = parser_screen_subparsers.add_parser('sliding')
    parser_screen_sliding.add_argument(
        '--sliding-h',
        default=600,
        type=int,
        help='Height of sliding window'
    )
    parser_screen_sliding.add_argument(
        '--save-to',
        default='out/sliding/',
        help='Folder to save screenshots'
    )

    parser_screen_full = parser_screen_subparsers.add_parser('full')
    parser_screen_full.add_argument(
        '--save-to',
        default='out/full/',
        help='Folder to save screenshots'
    )

    parser_screen_sections = parser_screen_subparsers.add_parser('sections')
    parser_screen_sections.add_argument(
        '--save-to',
        default='out/sections/',
        help='Folder to save screenshots'
    )

    return parser.parse_args()

def create_driver():
    dcap = dict(DesiredCapabilities.PHANTOMJS)
    dcap["phantomjs.page.settings.userAgent"] = USER_AGENT
    
    return webdriver.PhantomJS(desired_capabilities=dcap)

def fetch_urls(driver, ignore=None, dry_run=None):
    for fetcher, category, source in FETCHERS_LIST:
        is_ignored = False

        if ignore:
            for _source, _category in ignore:
                if (_source == source or _source == '*') and (_category == category or _category == '*'):
                    is_ignored = True
                    break

        if is_ignored:
            print(f'Skip (ignored) {source}:{category}')
            continue

        if dry_run:
            print(f'Processing: {source}:{category} - dry run')
            continue

        links = fetcher(driver)

        for i, link in enumerate(links):
            print(f'Processing: {source}:{category} - {i}', end='\r')
            Site.objects.insert([Site(url=link, category=category, source=source)])

        print(f'Processing {source}:{category} - done ({i} sites)')

def screen_sections(driver, save_to):
    assert isinstance(save_to, str)

    if not os.path.exists(save_to):
        os.makedirs(save_to)

    for site in Site.objects(is_screened_sections__ne=True):
        screener.screen_sections(driver, site.url)
        site.is_screened_sections = True
        site.save()

def screen_full_page(driver, save_to, scroll_to_bottom, no_mark_saved):
    assert isinstance(save_to, str)
    assert isinstance(scroll_to_bottom, bool)
    assert isinstance(no_mark_saved, bool)

    if not os.path.exists(save_to):
        os.makedirs(save_to)

    for site in Site.objects(is_screened_full__ne=True):
        screener.screen_full_page(driver, site.url, save_to=save_to, enable_scroll_to_bottom=scroll_to_bottom)
        
        if not no_mark_saved:
            site.is_screened_full = True
            site.save()

def screen_sliding_window(driver, sliding_h, save_to, no_mark_saved, scroll_to_bottom):
    assert isinstance(save_to, str)
    assert isinstance(sliding_h, int)
    assert isinstance(no_mark_saved, bool)
    assert isinstance(scroll_to_bottom, bool)

    if not os.path.exists(save_to):
        os.makedirs(save_to)

    for site in Site.objects(is_screened_sliding__ne=True):
        screener.screen_sliding_window(
            driver, 
            site.url, 
            sliding_h=sliding_h, 
            save_to=save_to,
            enable_scroll_to_bottom=scroll_to_bottom
        )
        
        if not no_mark_saved:
            site.is_screened_sliding = True
            site.save()

if __name__ == '__main__':
    args = parse_args()

    driver = create_driver()
    driver.set_window_size(*BROWSER_SIZE)

    if args.mode == 'fetch_urls':
        fetch_urls(driver, args.ignore, args.dry_run)
    elif args.mode == 'screen':
        if args.screen_mode == 'sliding':
            screen_sliding_window(
                driver, 
                sliding_h=args.sliding_h, 
                save_to=args.save_to,
                no_mark_saved=args.no_mark_saved,
                scroll_to_bottom=not args.no_scroll_bottom
            )
        elif args.screen_mode == 'full':
            screen_full_page(
                driver, 
                save_to=args.save_to, 
                scroll_to_bottom=not args.no_scroll_bottom,
                no_mark_saved=args.no_mark_saved
            )
        elif args.screen_mode == 'sections':
            screen_sections(driver, save_to=args.save_to)
