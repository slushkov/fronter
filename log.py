import os
from logging import getLogger, FileHandler, StreamHandler, Logger, DEBUG, INFO, WARNING, Formatter


def _create_logger(name, filename, level, logs_folder='logs/'):
    assert isinstance(name, str)
    assert isinstance(filename, str)
    assert level in (DEBUG, INFO, WARNING)
    assert isinstance(logs_folder, str)
    assert os.path.exists(logs_folder)

    FORMAT = '%(levelname)-8s %(message)s'

    logger = getLogger(name)
    fh = FileHandler(os.path.join(logs_folder, filename))
    fh.setFormatter(Formatter(FORMAT))
    sh = StreamHandler()
    sh.setFormatter(Formatter(FORMAT))
    logger.addHandler(fh)
    logger.addHandler(sh)
    logger.setLevel(level)

    return logger

def _indent_logger(logger, value):
    assert isinstance(logger, Logger)
    assert isinstance(value, int)

    for handler in logger.handlers:
        assert handler.formatter
        assert handler.formatter._fmt.count('%(message)') == 1

        fmt = handler.formatter._fmt
        _initial_len = len(fmt)

        assert isinstance(fmt, str)

        if value > 0:
            fmt = fmt.replace('%(message)', ' ' * value + '%(message)')
        elif value < 0:
            fmt = fmt.replace(' ' * abs(value) + '%(message)', '%(message)')

        assert isinstance(fmt, str)
        assert len(fmt) == _initial_len + value

        handler.setFormatter(Formatter(fmt))

def indent_logger(logger):
    _indent_logger(logger, 2)

def unindent_logger(logger):
    _indent_logger(logger, -2)

fetch_logger = _create_logger('url_fetch', 'url_fetch.log', WARNING)
screener_logger = _create_logger('screener', 'screener.log', DEBUG)
