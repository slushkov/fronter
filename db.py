from mongoengine import connect, StringField, Document, URLField, IntField, BooleanField, ReferenceField, EmbeddedDocumentField, FloatField, ListField, EmbeddedDocument

from settings import MONGO_DB_NAME


connect(MONGO_DB_NAME)

class Site(Document):
    url = URLField(required=True)
    category = StringField()
    source = StringField()
    is_screened_full = BooleanField(default=False)
    is_screened_sections = BooleanField(default=False)
    is_screened_sliding = BooleanField(default=False)
