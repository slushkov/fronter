BROWSER_SIZE = (1440, 1035)
MONGO_DB_NAME = 'fronter'

USER_AGENT = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:57.0) Gecko/20100101 Firefox/57.0'

CSS_PROPS = [
    'background-color'
    'border-bottom-color'
    'border-bottom-style'
    'border-bottom-width'
    'border-left-color',
    'border-left-style',
    'border-left-width',
    'border-right-color',
    'border-right-style',
    'border-right-width',
    'border-top-color',
    'border-top-style',
    'border-top-width',
    'clear',
    'color',
    'display',
    'float',
    'height',
    'left',
    'letter-spacing',
    'line-height',
    'margin-bottom',
    'margin-left',
    'margin-right',
    'margin-top',
    'padding-bottom',
    'padding-left',
    'padding-right',
    'padding-top',
    'position',
    'text-align',
    'text-decoration',
    'text-indent',
    'text-transform',
    'top',
    'vertical-align',
    'width',
    'z-index',
    'align-content',
    'align-items',
    'align-self',
    'flex-basis',
    'flex-direction',
    'flex-flow',
    'flex-grow',
    'flex-shrink',
    'flex-wrap',
    'justify-content',
    'order',
    'visibility',
]
