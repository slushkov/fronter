import os
from base64 import b64decode
import math
import time

from selenium.webdriver.common.by import By
from selenium.webdriver.remote.webelement import WebElement
from selenium.webdriver import ActionChains
from wand.image import Image

from log import screener_logger as logger, indent_logger, unindent_logger
from settings import BROWSER_SIZE


class BadElementSizeError(Exception):
    pass

class Timeit(object):
    def __enter__(self):
        self.start_t = time.time()
        return lambda: round(time.time() - self.start_t, 2)
    def __exit__(self, type, value, traceback):
        pass

def url_to_filename(url):
    assert isinstance(url, str)
    assert url.startswith('http')

    url = url.split('?')[0] # Remove query part

    replaces = {
        '-': '_',
        'http://': '',
        'https://': '',
        '.': '_',
        '/': '_'
    }

    filename = url

    for what, replace_on in replaces.items():
        filename = filename.replace(what, replace_on)

    return filename

def focus_on_element(driver, elemet):
    ActionChains(driver).move_to_element(element).perform()  # focus
    driver.execute_script("arguments[0].scrollIntoView();", element)

def save_element_screenshot(screenshot, element, path):
    driver = element._parent
    
    _convert_start_t = time.time()
    
    scr_img = Image(blob=screenshot)

    logger.debug(f'Convert done ({round(time.time() - _convert_start_t, 2)} s)')

    x = element.location["x"]
    y = element.location["y"]
    w = element.size["width"]
    h = element.size["height"]

    _crop_start_t = time.time()

    if w == 0 or h == 0:
        raise BadElementSizeError()

    scr_img.crop(
        left=x,
        top=y,
        width=min(w, scr_img.size[0]),
        height=min(h, scr_img.size[1]),
    )

    logger.debug(f'Croped ({round(time.time() - _crop_start_t, 2)} s)')

    _save_start_t = time.time()
    
    scr_img.save(filename=path)
    logger.debug(f'Saved: {path} ({round(time.time() - _save_start_t, 2)} s)')

def scroll_to_bottom(driver):
    with Timeit() as get_elapsed:
        driver.execute_script('window.scrollTo(0,document.body.scrollHeight);')
        logger.debug(f'Scroll to bottom done ({get_elapsed()} s)')

def get_page(driver, url):
    assert isinstance(url, str)
    assert url.startswith('http')

    with Timeit() as elapsed:
        driver.get(url)
        logger.debug(f'Page {url} loaded ({elapsed()} s)')

def screen_sliding_window(driver, url, sliding_h, save_to, enable_scroll_to_bottom):
    assert isinstance(url, str)
    assert url.startswith('http')
    assert isinstance(sliding_h, int)
    assert isinstance(save_to, str)
    assert os.path.exists(save_to)

    with Timeit() as total_time:
        logger.info(f'Started sliding screening: {url}')
        logger.debug(f'Func params: save_to={save_to}, sliding_h={sliding_h}, enable_scroll_to_bottom={enable_scroll_to_bottom}')
        indent_logger(logger)
    
        get_page(driver, url)

        logger.debug(f'Scroll to bottom feature: {"enabled" if enable_scroll_to_bottom else "disabled"}')
        if enable_scroll_to_bottom:
            scroll_to_bottom(driver)

        screen = Image(blob=b64decode(driver.get_screenshot_as_base64()))

        assert screen.size[1] > sliding_h

        windows_count = math.ceil(screen.size[1] / sliding_h)

        logger.debug(f'Slides count: {windows_count} (page height: {screen.size[1]})')

        for wnd_idx in range(windows_count):
            bottom = min((wnd_idx + 1) * sliding_h, screen.size[1])
            top = bottom - sliding_h

            filepath = os.path.join(save_to, f'{url_to_filename(url)}_{wnd_idx}.png')

            with screen[:, top:bottom] as sliding_window:
                sliding_window.save(filename=filepath)
                logger.debug(f'Saved sliding window {wnd_idx}/{windows_count}: {filepath}')

        unindent_logger(logger)
        logger.info(f'Finished sliding screening: {url} ({total_time()} s)')

def screen_full_page(driver, url, save_to, enable_scroll_to_bottom=True):
    assert isinstance(url, str)
    assert url.startswith('http')
    assert isinstance(save_to, str)
    assert os.path.exists(save_to)

    with Timeit() as total_time:
        logger.info(f'Started full page screen: {url}')
        logger.debug(f'Func params: save_to={save_to}')
        indent_logger(logger)

        get_page(driver, url)

        logger.debug(f'Scroll to bottom feature: {"enabled" if enable_scroll_to_bottom else "disabled"}')
        if enable_scroll_to_bottom:
            scroll_to_bottom(driver)
        
        filepath = os.path.join(save_to, f'{url_to_filename(url)}.png')
        
        with Timeit() as elapsed:
            driver.save_screenshot(filepath)
            logger.debug(f'Screen saved: {filepath} ({elapsed()} s)')

        unindent_logger(logger)
        logger.info(f'Finished full page screen: {url} (total: {total_time()} s)')

def screen_sections(driver, url, save_to):
    assert isinstance(url, str)
    assert url.startswith('http')
    assert isinstance(save_to, str)
    assert os.path.exists(save_to)

    logger.info(f'Started site: {url}')
    indent_logger(logger, 2)

    _site_start_t = time.time()
    
    driver.get(url)
    logger.debug('Page GET finished')

    _after_get_t = time.time()

    url_filename = url_to_filename(url)

    sections = driver.find_elements(By.XPATH, '//section')
    logger.debug(f'Found {len(sections)} sections')

    screenshot = b64decode(driver.get_screenshot_as_base64())

    for i, section in enumerate(sections):
        indent_logger(logger, 2)
        
        _section_start_t = time.time()

        try:
            indent_logger(logger, 2)
            save_element_screenshot(screenshot, section, os.path.join(save_to, f'{url_filename}_{i}.png'))
            indent_logger(logger, -2)
        except BadElementSizeError:
            indent_logger(logger, -2)
            logger.info(f'Section {i} skipped: bad size')
        except Exception as e:
            indent_logger(logger, -2)
            logger.info(f'Section {i} screen error: {e}') 
        else:
            logger.debug(f'Saved section {i}/{len(sections)} screenshot ({round(time.time() - _section_start_t, 2)} s)')

        indent_logger(logger, -2)

    indent_logger(logger, -2)
    logger.info(f'Finished site: {url} (total: {round(time.time() - _site_start_t, 2)} s, screening: {round(time.time() - _after_get_t, 2)} s)')
